package com.example.lecture8

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    fun init()
    {
        val cont=myContext()
        login.setOnClickListener {

            val mail=email.text.toString()
            val pas=password.text.toString()

            //checking email validation
            if(!isEmailValid(mail))  Toast.makeText(this,"please enter correct E-MAIL adress",Toast.LENGTH_SHORT).show()

            //check if all fields are filled
            else if(mail.isEmpty() || pas.isEmpty() ) Toast.makeText(this,"all fields are required",Toast.LENGTH_SHORT).show()

            //check if password and mail is valid
            else  if(mail == cont.Email && pas==cont.password ) Toast.makeText(this,"logged in successfully ",Toast.LENGTH_SHORT).show()

            else if(mail !=cont.Email || pas!=cont.password)    Toast.makeText(this,"password or mail is incorrect ",Toast.LENGTH_SHORT).show()
        }


    }
    //email validation
    fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
}
